import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router';
import {reduxForm, Field} from 'redux-form';
import { connect } from 'react-redux';


import Messages from '../notifications/Messages';
import Errors from '../notifications/Errors';

import signupRequest from './actions';

class Signup extends Component {

    static propTypes = {
        handleSubmit: PropTypes.func,
        singupRequest: PropTypes.func,
        singup: PropTypes.shape({
            requesting: PropTypes.bool,
            successful: PropTypes.bool,
            messages: PropTypes.array,
            errors: PropTypes.array
        }),
    }

    submit = (values) => {
        this.props.signupRequest(values);
    }

    render(){

        const {
            handleSubmit,
            signup: {
                requesting,
                successful,
                messages,
                errors,
            }
        } = this.props;

        return (
        <div className="signup">
            <form className="widget-form" onSubmit={handleSubmit(this.submit)}>
                <h1>Signup</h1>
                <label htmlFor="username">Username</label>
                <Field 
                  name="username"
                  type="text"
                  id="username"
                  className="email"
                  label="Username"
                  component="input"
                />
                <label htmlFor="email">Email</label>
                <Field 
                  name="email"
                  type="text"
                  id="email"
                  className="email"
                  label="Email"
                  component="input"
                />
                <label htmlFor="plainPassword.first">Password</label>
                <Field 
                  name="plainPassword.first"
                  type="password"
                  id="plainPassword.first"
                  className="password"
                  label="Password"
                  component="input"
                />
                <label htmlFor="plainPassword.second">Confirm Password</label>
                <Field 
                  name="plainPassword.second"
                  type="password"
                  id="plainPassword.second"
                  className="password"
                  label="Confirm Password"
                  component="input"
                />
                <button action="submit">SIGNUP</button>
            </form>
            <div className="auth-messages">
                {!requesting && !!errors.length && (
                    <Errors messages="Failure to signup due to:" errors={errors} />
                )}
                { !requesting && successful && (
                    <div>
                        Signup Successful! <Link to="/login">Click here to Login</Link>
                    </div>
                )}
                { !requesting && !successful &&(
                    <Link to="/login">Already a Widgeter? Login Here >></Link>
                )}
            </div>
        </div>
        )
    }
};

const mapStateToProps = state => ({
    signup: state.signup,
});

const connected = connect(mapStateToProps, {signupRequest})(Signup);

const formed = reduxForm({
    form: 'signup',
})(connected)

export default formed;
