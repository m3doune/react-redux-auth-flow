import { SIGNUP_REQUESTING } from './constants';

const signupRequest = function signupRequest({username, email, plainPassword}){
    return {
        type: SIGNUP_REQUESTING,
        username,
        email,
        plainPassword,
    }
}

export default signupRequest;