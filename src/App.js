import React from 'react';
import Proptypes from 'prop-types';
import logo from './logo.svg';
import './App.css';

const App = props => (
  <div className="App">
    <div className="App-header">
      <img src={logo} alt="logo" className="App-logo"/>
      <h2> Welcome to Widget Reactory </h2>
    </div>
    <section className="App-body">
      {props.children}
    </section>
  </div>
)

App.propTypes =  {
  children: Proptypes.node,
}

export default App;
