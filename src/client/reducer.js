import { CLIENT_SET, CLIENT_UNSET } from './constants';

const initialState = {
    message: null,
    token: null,
    email: null
}

const reducer = function clientReducer(state = initialState, action) {
    switch(action.type) {
        case CLIENT_SET:
            return{
                message: action.message,
                token: action.token,
                email: action.email
            }
        case CLIENT_UNSET:
            return {
                message: null,
                token: null,
                email: null
            }
        default:
            return state;
    }
}

export default reducer;